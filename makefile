# Makefile for preparing the GetAGriffin Web app for deployment.
# This should be run in the build environment.
# Before running this, you must set the environmnet variables that are defined in buildenv.sh

.PHONY: resolve getdriver getjson getorderstub

.DEFAULT_GOAL: all

all: getdriver getjson getorderstub

resolve:
	$(MVN) dependency:resolve
	mkdir -p ./tomcat/webapps/example-webapp/WEB-INF/lib

# Obtain the JDBC driver needed by MySQL:
getdriver: resolve
	cp $(MVN_REPOSITORY_PATH)/mysql/mysql-connector-java/8.0.18/mysql-connector-java-8.0.18.jar \
		./tomcat/webapps/example-webapp/WEB-INF/lib/

# Obtain the com.google.gson jar:com.google.code.gson
getjson: resolve
	cp $(MVN_REPOSITORY_PATH)/com/google/code/gson/gson/2.8.6/gson-2.8.6.jar \
		./tomcat/webapps/GetAGriffin/WEB-INF/lib/

# Obtain the getagriffin.order.stub jar:
getorderstub: resolve
	cp $(MVN_REPOSITORY_PATH)/com/cliffberg/getagriffin.order.stub/1.0/getagriffin.order.stub-1.0.jar \
		./tomcat/webapps/GetAGriffin/WEB-INF/lib/
